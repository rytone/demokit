#version 330 core

uniform float time;
uniform vec2 res;

uniform sampler2D inColor;
uniform sampler2D inDepth;

out vec4 outColor;

void main() {
    float depth = 1.0/texture(inDepth, gl_FragCoord.xy/res).r-1.0;

    float aperature = 0.3;
    float focal_length = 0.6;
    float plane_in_focus = 20.0;

    float coc = abs(aperature * (focal_length * (depth - plane_in_focus)) / (depth * (plane_in_focus - focal_length)));
    float blur_radius = coc * res.y;

    vec3 blur_sum = vec3(0);
    float div = 0;
    for (float y=-blur_radius; y<=blur_radius; y++) {
        for (float x=-blur_radius; x<=blur_radius; x++) {
            div++;
            blur_sum += texture(inColor, (gl_FragCoord.xy + vec2(x, y))/res).rgb;
        }
    }
    blur_sum /= div;

    outColor = vec4(blur_sum, 1.0);
}