#[macro_use]
extern crate demokit;
#[macro_use]
extern crate glium;
#[macro_use]
extern crate log;
extern crate pretty_env_logger;

use std::fmt;
use std::rc::Rc;

use demokit::assets::{Asset, ShaderAsset};
use demokit::demo::{backend, DemoBuilder, DemoContext};
use demokit::pipeline::{Pipeline, Stage};
use demokit::util::{primitive, BlitTexture, ClearDisplay, FinishDisplay};

use glium::framebuffer::SimpleFrameBuffer;
use glium::texture::{DepthTexture2d, Texture2d};
use glium::{IndexBuffer, Surface, VertexBuffer};

struct MainRender {
    vbuf: VertexBuffer<primitive::PrimitiveVertex>,
    ibuf: IndexBuffer<u32>,

    shader: ShaderAsset,

    color_tex: Rc<Texture2d>,
    depth_tex: Rc<DepthTexture2d>,
}

impl MainRender {
    fn new(ctx: &mut DemoContext) -> Self {
        let (vbuf, ibuf) = primitive::quad(&ctx.gl);

        let mut shader = packed_shader!("main_shader");
        ctx.reloader
            .watch(&mut shader, "examples/main_shader")
            .unwrap();
        match shader.compile(&ctx.gl) {
            Ok(_) => debug!("shader compiled successfully"),
            Err(e) => {
                error!("shader failed to compile!\n      {}", e);
                panic!("error while loading demo");
            }
        };

        let (res_x, res_y) = ctx.target_res;
        let color_tex = Rc::new(Texture2d::empty(&ctx.gl, res_x, res_y).unwrap());
        let depth_tex = Rc::new(DepthTexture2d::empty(&ctx.gl, res_x, res_y).unwrap());

        MainRender {
            vbuf,
            ibuf,
            shader,
            color_tex,
            depth_tex,
        }
    }
}

impl fmt::Display for MainRender {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "MainRender")
    }
}

impl Stage for MainRender {
    type Input = ();
    type Output = (Rc<Texture2d>, Rc<DepthTexture2d>);

    fn exec(&mut self, ctx: &mut DemoContext, _: Self::Input) -> Self::Output {
        if let Err(e) = ctx.reloader
            .reload(&mut self.shader, |shader| shader.compile(&ctx.gl))
        {
            error!("failed to reload shader: {}", e);
        }

        {
            let (res_x, res_y) = self.color_tex.dimensions();

            let mut framebuffer = SimpleFrameBuffer::with_depth_buffer(
                &ctx.gl,
                self.color_tex.as_ref(),
                self.depth_tex.as_ref(),
            ).unwrap();
            framebuffer
                .draw(
                    &self.vbuf,
                    &self.ibuf,
                    self.shader.get_shader().unwrap(),
                    &uniform! {
                        time: ctx.time.beats as f32,
                        res: [res_x as f32, res_y as f32],
                    },
                    &glium::DrawParameters {
                        depth: glium::Depth {
                            test: glium::DepthTest::Overwrite,
                            write: true,
                            ..Default::default()
                        },
                        ..Default::default()
                    },
                )
                .unwrap();
        }

        (Rc::clone(&self.color_tex), Rc::clone(&self.depth_tex))
    }
}

struct PostDOF {
    vbuf: VertexBuffer<primitive::PrimitiveVertex>,
    ibuf: IndexBuffer<u32>,

    shader: ShaderAsset,

    color_tex: Rc<Texture2d>,
}

impl PostDOF {
    fn new(ctx: &mut DemoContext) -> Self {
        let (vbuf, ibuf) = primitive::quad(&ctx.gl);

        let mut shader = packed_shader!("post/dof");
        ctx.reloader
            .watch(&mut shader, "examples/post/dof")
            .unwrap();
        match shader.compile(&ctx.gl) {
            Ok(_) => debug!("shader compiled successfully"),
            Err(e) => {
                error!("shader failed to compile!\n      {}", e);
                panic!("error while loading demo");
            }
        };

        let (res_x, res_y) = ctx.target_res;
        let color_tex = Rc::new(Texture2d::empty(&ctx.gl, res_x, res_y).unwrap());

        PostDOF {
            vbuf,
            ibuf,
            shader,
            color_tex,
        }
    }
}

impl fmt::Display for PostDOF {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DOF")
    }
}

impl Stage for PostDOF {
    type Input = (Rc<Texture2d>, Rc<DepthTexture2d>);
    type Output = Rc<Texture2d>;

    fn exec(&mut self, ctx: &mut DemoContext, (in_color, in_depth): Self::Input) -> Self::Output {
        if let Err(e) = ctx.reloader
            .reload(&mut self.shader, |shader| shader.compile(&ctx.gl))
        {
            error!("failed to reload shader: {}", e);
        }

        {
            let (res_x, res_y) = self.color_tex.dimensions();

            let mut framebuffer = SimpleFrameBuffer::new(&ctx.gl, self.color_tex.as_ref()).unwrap();
            framebuffer
                .draw(
                    &self.vbuf,
                    &self.ibuf,
                    self.shader.get_shader().unwrap(),
                    &uniform! {
                        time: ctx.time.beats as f32,
                        res: [res_x as f32, res_y as f32],
                        inColor: in_color.as_ref(),
                        inDepth: in_depth.as_ref(),
                    },
                    &Default::default(),
                )
                .unwrap();
        }

        Rc::clone(&self.color_tex)
    }
}

fn main() {
    pretty_env_logger::init();
    let demo = DemoBuilder::from_pipeline(
        Pipeline::new(ClearDisplay::new)
            .then(MainRender::new)
            .then(Pipeline::new(PostDOF::new).named("Post"))
            .then(BlitTexture::default)
            .then(FinishDisplay::new),
    ).title("block wave")
        .author("max")
        .with_backend(backend::DebugBackend);

    match demo.run() {
        Ok(_) => (),
        Err(e) => error!("Failed to run the demo: {}", e),
    }
}
