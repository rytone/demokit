#version 330 core

uniform float time;
uniform vec2 res;

out vec4 outColor;

#define EPSILON 0.001
#define FAR 48.0
#define MAX_STEPS 64

float vmax(vec3 v) {
	return max(max(v.x, v.y), v.z);
}

// Plane with normal n (n is normalized) at some distance from the origin
float fPlane(vec3 p, vec3 n, float distanceFromOrigin) {
	return dot(p, n) + distanceFromOrigin;
}

// Cheap Box: distance to corners is overestimated
float fBoxCheap(vec3 p, vec3 b) { //cheap box
	return vmax(abs(p) - b);
}

// Rotate around a coordinate axis (i.e. in a plane perpendicular to that axis) by angle <a>.
// Read like this: R(p.xz, a) rotates "x towards z".
// This is fast if <a> is a compile-time constant and slower (but still practical) if not.
void pR(inout vec2 p, float a) {
	p = cos(a)*p + sin(a)*vec2(p.y, -p.x);
}

// Repeat in two dimensions
vec2 pMod2(inout vec2 p, vec2 size) {
	vec2 c = floor((p + size*0.5)/size);
	p = mod(p + size*0.5,size) - size*0.5;
	return c;
}

float map(vec3 p) {
    float rep_len = 6.0;

    float d = fPlane(p, vec3(0,1,0), 0.0);

    vec3 round_p = round(p/rep_len);
    vec3 rep_p = p;
    float phase = (round_p.x - round_p.z)*0.3;
    float bob = (sin(time + phase)*2.5+1.0)+(sin(time*4.5+phase));
    rep_p -= vec3(0,bob,0);
    pMod2(rep_p.xz, vec2(rep_len));
    //pR(rep_p.xy, time*-0.7);
    //pR(rep_p.xz, time);
    pR(rep_p.yz, time*-0.5 + round_p.x*2.0);

    d = min(d, fBoxCheap(rep_p, vec3(1.0)));

    return d;
}

float march_map(vec3 eye, vec3 dir) {
    float depth = 0.0;
    vec3 pos = eye;
    
    for (int i = 0; i < MAX_STEPS; i++) {
        float d = map(pos);
        depth += d;
        pos += dir * d;
        
        if (d < EPSILON) {
            break;
        }
        
        if (d >= FAR) {
            return FAR;
        }
    }
    
    return depth;
}

vec3 map_nrm(vec3 p) {
    vec2 e = vec2(0.005, -0.005); 
    return normalize(e.xyy * map(p + e.xyy) + e.yyx * map(p + e.yyx) + e.yxy * map(p + e.yxy) + e.xxx * map(p + e.xxx));
}

vec3 color(vec3 eye, vec3 dir, float depth) {
    vec3 sky = vec3(1.0);
    if (depth >= FAR) {
        return sky;
    } else {
        vec3 hit_pos = eye + dir * depth;
        vec3 nrm = map_nrm(hit_pos);
        
        vec3 color = mix(sky, vec3(230.0/255.0, 228.0/255.0, 176.0/255.0), nrm.x);
        color = mix(color, vec3(130.0/255.0, 186.0/255.0, 180.0/255.0), nrm.y);
        color = mix(color, vec3(83.0/255.0, 84.0/255.0, 132.0/255.0), nrm.z);

        vec3 shadow_start = hit_pos + nrm*(map(hit_pos)*5.0);
        float shadow_depth = march_map(shadow_start, normalize(vec3(0.4,1.0,-0.3)));
        if (shadow_depth < FAR && floor(nrm+vec3(0.0001))==vec3(0,1,0)) color *= 0.6;
        
        return color;
    }
}

void main() {
    vec3 eye = vec3(15.0) + vec3(time*2.0, 0, -time*2.0);
    vec3 look = vec3(0.0) + vec3(time*2.0, 0, -time*2.0);
    float scale = 15.0;
    
    vec3 centerdir = normalize(look - eye);
    vec3 right = normalize(cross(centerdir, vec3(0.0, 1.0, 0.0)));
    vec3 up = normalize(cross(right, centerdir));
    
    vec2 fragCoordPolar = gl_FragCoord.xy - res.xy * 0.5;
    fragCoordPolar /= res.y;
    fragCoordPolar *= scale;
    
    vec3 realeye = eye + right * fragCoordPolar.x + up * fragCoordPolar.y;
    
    float depth = march_map(realeye, centerdir);
    outColor = vec4(color(realeye, centerdir, depth), 1.0);
    gl_FragDepth = 1.0/(depth + 1.0);
}