use std::io;
use std::path::PathBuf;
use std::sync::mpsc::{channel, Receiver, TryRecvError};
use std::time::Duration;

use notify;
use notify::{DebouncedEvent, RecommendedWatcher, RecursiveMode, Watcher};

/// A wrapper over `notify::Watcher`, simplifying it down to a single `poll`
/// method.
pub struct ReloadWatcher {
	_notify_watcher: RecommendedWatcher,
	watcher_rx: Receiver<DebouncedEvent>,
}

impl ReloadWatcher {
	/// Constructs a new `ReloadWatcher`, automatically watching the specified
	/// path for changes.
	///
	/// # Arguments
	///
	/// * `path` - The path to the file or directory to watch relative to the
	/// current working directory.
	pub fn new(path: &str) -> notify::Result<Self> {
		let (tx, rx) = channel();
		let mut watcher: RecommendedWatcher = Watcher::new(tx, Duration::from_millis(500))?;
		watcher.watch(path, RecursiveMode::NonRecursive)?;

		Ok(ReloadWatcher {
			_notify_watcher: watcher,
			watcher_rx: rx,
		})
	}

	/// Polls the `ReloadWatcher` for changes, returning the path to the latest
	/// changed file.
	pub fn poll(&self) -> Option<PathBuf> {
		match self.watcher_rx.try_recv() {
			Ok(e) => match e {
				DebouncedEvent::Write(path) => Some(path),
				_ => None,
			},
			Err(TryRecvError::Empty) => None,
			Err(TryRecvError::Disconnected) => panic!("watcher rx disconnected!"),
		}
	}
}

/// The result of a reload poll (used for `Asset::hot_reload`)
pub enum ReloadResult {
	/// The asset was reloaded.
	Reload,
	/// The asset was not reloaded.
	NoReload,
}

quick_error! {
	#[derive(Debug)]
	pub enum ReloadError {
		AssetNotWatched {
			description("The asset is not being watched cannot be reloaded.")
		}
		Io(err: io::Error) {
			cause(err)
			description(err.description())
			from()
		}
	}
}
