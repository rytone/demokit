use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;

use glium::backend::Facade;
use glium::texture::{RawImage2d, Texture2d, TextureCreationError};

use image;
use notify;

use assets::{Asset, ReloadWatcher};

quick_error! {
    #[derive(Debug)]
    pub enum ImageError {
        Image(err: image::ImageError) {
            cause(err)
            description(err.description())
            display("Failed to load the image: {}", err)
            from()
        }
        TextureCreation(err: TextureCreationError) {
            cause(err)
            description(err.description())
            display("Failed to create the texture: {}", err)
            from()
        }
    }
}

pub struct ImageAsset {
    data: Vec<u8>,
    reload_watcher: Option<ReloadWatcher>,
    texture: Option<Texture2d>,
}

impl Asset<Vec<u8>> for ImageAsset {
    fn from_data(data: Vec<u8>) -> Self {
        ImageAsset {
            data: data,
            reload_watcher: None,
            texture: None,
        }
    }

    fn load_data<P: AsRef<Path>>(path: P) -> io::Result<Vec<u8>> {
        let file = File::open(path)?;
        let bytes_result: io::Result<Vec<u8>> = file.bytes().collect();

        Ok(bytes_result?)
    }

    fn get_data(&self) -> &Vec<u8> {
        &self.data
    }

    fn set_data(&mut self, data: Vec<u8>) {
        self.data = data
    }

    fn watch(&mut self, path: &str) -> notify::Result<()> {
        self.reload_watcher = Some(ReloadWatcher::new(path)?);
        Ok(())
    }

    fn watcher(&self) -> Option<&ReloadWatcher> {
        self.reload_watcher.as_ref()
    }
}

impl ImageAsset {
    pub fn load_texture<F: Facade>(&mut self, facade: &F) -> Result<(), ImageError> {
        let image = image::load_from_memory(self.get_data().as_slice())?.to_rgba();
        let dimensions = image.dimensions();

        let raw_image = RawImage2d::from_raw_rgba_reversed(&image.into_raw(), dimensions);
        self.texture = Some(Texture2d::new(facade, raw_image).map_err(ImageError::from)?);

        Ok(())
    }

    pub fn get_texture(&self) -> Option<&Texture2d> {
        self.texture.as_ref()
    }
}
