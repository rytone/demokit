/// Creates an asset from a file that is packed into the final executable.
///
/// # Arguments
///
/// * `path` - Path to the the asset file relative to the source file
/// `packed_asset` is called from.
#[macro_export]
macro_rules! packed_asset {
	($path:expr) => {{
		::demokit::assets::Asset::from_data(include_bytes!($path).to_vec())
		}};
}

/// Creates a shader asset that is packed into the final executable.
///
/// # Arguments
///
/// * `path` - Path to the folder containing the vertex and fragment shaders
/// relative to the source file `packed_shader` is called from.
///
/// # Note
///
/// The shader folder *must* contain `vertex.glsl` and `fragment.glsl`,
/// this macro will error at compile time otherwise.
#[macro_export]
macro_rules! packed_shader {
	($path:expr) => {{
		::demokit::assets::ShaderAsset::from_data(::demokit::assets::shader::ShaderAssetData {
			vertex: String::from_utf8(include_bytes!(concat!($path, "/vertex.glsl")).to_vec())
				.unwrap(),
			fragment: String::from_utf8(include_bytes!(concat!($path, "/fragment.glsl")).to_vec())
				.unwrap(),
			geometry: None,
			})
		}};
}

/// Creates a shader asset including a geometry shader that is packed into the
/// final executable.
///
/// # Arguments
///
/// * `path` - Path to the folder containing the vertex, fragment, and geometry
/// shaders relative to the source file `packed_shader` is called from.
///
/// # Note
///
/// The shader folder *must* contain `vertex.glsl`, `fragment.glsl`, and
/// geometry.glsl`, this macro will error at compile time otherwise.
#[macro_export]
macro_rules! packed_shader_with_geom {
	($path:expr) => {{
		::demokit::assets::ShaderAsset::from_data(::demokit::assets::shader::ShaderAssetData {
			vertex: String::from_utf8(include_bytes!(concat!($path, "/vertex.glsl")).to_vec())
				.unwrap(),
			fragment: String::from_utf8(include_bytes!(concat!($path, "/fragment.glsl")).to_vec())
				.unwrap(),
			geometry: Some(
				String::from_utf8(include_bytes!(concat!($path, "/geometry.glsl")).to_vec())
					.unwrap(),
			),
			})
		}};
}
