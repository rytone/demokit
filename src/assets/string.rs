use std::io;
use std::io::Read;
use std::fs::File;
use std::path::Path;

use notify;

use assets::{Asset, ReloadWatcher};

// An asset for text.
pub struct StringAsset {
	data: String,
	reload_watcher: Option<ReloadWatcher>,
}

impl Asset<String> for StringAsset {
	fn from_data(data: String) -> Self {
		StringAsset {
			data: data,
			reload_watcher: None,
		}
	}

	fn load_data<P: AsRef<Path>>(path: P) -> io::Result<String> {
		let mut file = File::open(path)?;
		let mut buf = String::new();
		file.read_to_string(&mut buf)?;
		Ok(buf)
	}

	fn get_data(&self) -> &String {
		&self.data
	}

	fn set_data(&mut self, data: String) {
		self.data = data
	}

	fn watch(&mut self, path: &str) -> notify::Result<()> {
		self.reload_watcher = Some(ReloadWatcher::new(path)?);
		Ok(())
	}

	fn watcher(&self) -> Option<&ReloadWatcher> {
		self.reload_watcher.as_ref()
	}
}
