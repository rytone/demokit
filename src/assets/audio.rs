use std::io;
use std::io::{Cursor, Read};
use std::fs::File;
use std::path::Path;

use notify;
use rodio::Decoder;
use rodio::decoder::DecoderError;

use assets::{Asset, ReloadWatcher};

/// An asset for audio sources.
pub struct AudioAsset {
	data: Vec<u8>,
	reload_watcher: Option<ReloadWatcher>,
}

impl Asset<Vec<u8>> for AudioAsset {
	fn from_data(data: Vec<u8>) -> Self {
		AudioAsset {
			data: data,
			reload_watcher: None,
		}
	}

	fn load_data<P: AsRef<Path>>(path: P) -> io::Result<Vec<u8>> {
		let file = File::open(path)?;
		let bytes_result: io::Result<Vec<u8>> = file.bytes().collect();

		Ok(bytes_result?)
	}

	fn get_data(&self) -> &Vec<u8> {
		&self.data
	}

	fn set_data(&mut self, data: Vec<u8>) {
		self.data = data
	}

	fn watch(&mut self, path: &str) -> notify::Result<()> {
		self.reload_watcher = Some(ReloadWatcher::new(path)?);
		Ok(())
	}

	fn watcher(&self) -> Option<&ReloadWatcher> {
		self.reload_watcher.as_ref()
	}
}

impl AudioAsset {
	/// Converts the `AudioAsset` into a `rodio::Decoder`, consuming the AudioAsset.
	pub fn into_source(self) -> Result<Decoder<Cursor<Vec<u8>>>, DecoderError> {
		Decoder::new(Cursor::new(self.data))
	}

	/// Converts the `AudioAsset` into a `rodio::Decoder`, cloning the inner audio data.
	pub fn clone_into_source(&self) -> Result<Decoder<Cursor<Vec<u8>>>, DecoderError> {
		Decoder::new(Cursor::new(self.data.clone()))
	}
}
