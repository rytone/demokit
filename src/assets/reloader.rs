use std::error::Error;
use std::fmt;

use notify;

use assets::Asset;
use assets::watcher::{ReloadError, ReloadResult};

// quick_error not possible due to generic
#[derive(Debug)]
pub enum ReloaderError<T: Error> {
    Reload(ReloadError),
    User(T),
}

impl<T: Error> fmt::Display for ReloaderError<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ReloaderError::Reload(ref e) => e.fmt(f),
            ReloaderError::User(ref e) => fmt::Display::fmt(&e, f),
        }
    }
}

impl<T: Error> Error for ReloaderError<T> {
    fn description(&self) -> &str {
        match *self {
            ReloaderError::Reload(ref e) => e.description(),
            ReloaderError::User(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            ReloaderError::Reload(ref e) => Some(e),
            ReloaderError::User(ref e) => Some(e),
        }
    }
}

pub enum Reloader {
    Default,
    None,
}

impl Reloader {
    pub fn watch<D, T: Asset<D>>(&self, asset: &mut T, path: &str) -> notify::Result<()> {
        match *self {
            Reloader::Default => asset.watch(path),
            Reloader::None => Ok(()),
        }
    }

    pub fn reload<D, T: Asset<D>, E: Error, F: FnMut(&mut T) -> Result<(), E>>(
        &self,
        asset: &mut T,
        mut callback: F,
    ) -> Result<(), ReloaderError<E>> {
        match *self {
            Reloader::Default => match asset.hot_reload() {
                Ok(ReloadResult::Reload) => callback(asset).map_err(ReloaderError::User),
                Ok(_) => Ok(()),
                Err(e) => Err(ReloaderError::Reload(e)),
            },
            Reloader::None => Ok(()),
        }
    }
}
