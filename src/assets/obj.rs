use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;

use glium::backend::Facade;
use glium::index::PrimitiveType;
use glium::{self, IndexBuffer, VertexBuffer};
use notify;
use obj;

use assets::{Asset, ReloadWatcher};

#[derive(Copy, Clone)]
pub struct Vertex {
    pub position: [f32; 3],
    pub normal: [f32; 3],
}

impl From<obj::Vertex> for Vertex {
    fn from(f: obj::Vertex) -> Self {
        Vertex {
            position: f.position,
            normal: f.normal,
        }
    }
}

implement_vertex!(Vertex, position, normal);

pub type ObjBuffers = (VertexBuffer<Vertex>, IndexBuffer<u16>);

quick_error! {
    #[derive(Debug)]
    pub enum ObjError {
        Obj {
            description("Failed to parse the OBJ file.")
        }
        VertexBuffer(err: glium::vertex::BufferCreationError) {
            cause(err)
            description(err.description())
            display("Failed to create vertex buffer: {}", err)
            from()
        }
        IndexBuffer(err: glium::index::BufferCreationError) {
            cause(err)
            description(err.description())
            display("Failed to create index buffer: {}", err)
            from()
        }
    }
}

pub struct ObjAsset {
    data: Vec<u8>,
    reload_watcher: Option<ReloadWatcher>,
    buffers: Option<ObjBuffers>,
}

impl Asset<Vec<u8>> for ObjAsset {
    fn from_data(data: Vec<u8>) -> Self {
        ObjAsset {
            data: data,
            reload_watcher: None,
            buffers: None,
        }
    }

    fn load_data<P: AsRef<Path>>(path: P) -> io::Result<Vec<u8>> {
        let file = File::open(path)?;
        let bytes_result: io::Result<Vec<u8>> = file.bytes().collect();

        Ok(bytes_result?)
    }

    fn get_data(&self) -> &Vec<u8> {
        &self.data
    }

    fn set_data(&mut self, data: Vec<u8>) {
        self.data = data
    }

    fn watch(&mut self, path: &str) -> notify::Result<()> {
        self.reload_watcher = Some(ReloadWatcher::new(path)?);
        Ok(())
    }

    fn watcher(&self) -> Option<&ReloadWatcher> {
        self.reload_watcher.as_ref()
    }
}

impl ObjAsset {
    pub fn load_buffers<F: Facade>(&mut self, facade: &F) -> Result<(), ObjError> {
        let o: obj::Obj<obj::Vertex> =
            obj::load_obj(self.get_data().as_slice()).map_err(|_| ObjError::Obj)?;

        let vertices: Vec<Vertex> = o.vertices.into_iter().map(|v| v.into()).collect();

        let vertex_buffer = VertexBuffer::new(facade, &vertices)?;
        let index_buffer = IndexBuffer::new(facade, PrimitiveType::TrianglesList, &o.indices)?;

        self.buffers = Some((vertex_buffer, index_buffer));

        Ok(())
    }

    pub fn get_buffers(&self) -> Option<&ObjBuffers> {
        self.buffers.as_ref()
    }
}
