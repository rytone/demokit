use std::io;
use std::path::Path;

use notify;
use glium::Display;
use glium::program::{Program, ProgramCreationError, ProgramCreationInput};

use assets::{Asset, ReloadError, ReloadResult, ReloadWatcher, StringAsset};

/// Internal data for `ShaderAsset`.
pub struct ShaderAssetData {
    pub vertex: String,
    pub fragment: String,
    pub geometry: Option<String>,
}

// An asset for OpenGL shaders.
pub struct ShaderAsset {
    reload_watcher: Option<ReloadWatcher>,
    data: ShaderAssetData,
    compiled_shader: Option<Program>,
}

impl Asset<ShaderAssetData> for ShaderAsset {
    fn from_data(data: ShaderAssetData) -> Self {
        ShaderAsset {
            reload_watcher: None,
            data: data,
            compiled_shader: None,
        }
    }

    fn load_data<P: AsRef<Path>>(path: P) -> io::Result<ShaderAssetData> {
        let mut path = path.as_ref().to_path_buf();

        path.push("vertex.glsl");
        let vertex_data = StringAsset::load_data(&path)?;

        path.set_file_name("fragment.glsl");
        let fragment_data = StringAsset::load_data(&path)?;

        path.set_file_name("geometry.glsl");
        let geometry_data = StringAsset::load_data(&path).ok();

        Ok(ShaderAssetData {
            vertex: vertex_data,
            fragment: fragment_data,
            geometry: geometry_data,
        })
    }

    fn get_data(&self) -> &ShaderAssetData {
        &self.data
    }

    fn set_data(&mut self, data: ShaderAssetData) {
        self.data = data;
    }

    fn watch(&mut self, path: &str) -> notify::Result<()> {
        self.reload_watcher = Some(ReloadWatcher::new(path)?);
        Ok(())
    }

    fn watcher(&self) -> Option<&ReloadWatcher> {
        self.reload_watcher.as_ref()
    }

    fn hot_reload(&mut self) -> Result<ReloadResult, ReloadError> {
        let path = self.watcher().ok_or(ReloadError::AssetNotWatched)?.poll();
        if let Some(mut path) = path {
            path.pop();
            self.set_data(Self::load_data(path)?);
            return Ok(ReloadResult::Reload);
        }
        Ok(ReloadResult::NoReload)
    }
}

impl ShaderAsset {
    /// Compiles the shader using the given `glium::Display`.
    ///
    /// # Arguments
    /// * `gl` - A borrow to the `glium::Display` to compile the shader with.
    pub fn compile(&mut self, gl: &Display) -> Result<(), ProgramCreationError> {
        let program = Program::new(
            gl,
            ProgramCreationInput::SourceCode {
                vertex_shader: &self.data.vertex,
                fragment_shader: &self.data.fragment,
                geometry_shader: self.data.geometry.as_ref().map(|g| g.as_str()),
                tessellation_control_shader: None,
                tessellation_evaluation_shader: None,
                transform_feedback_varyings: None,
                outputs_srgb: true,
                uses_point_size: false,
            },
        );
        self.compiled_shader = Some(program?);
        Ok(())
    }

    /// Returns a borrow to the compiled shader if it has been already compiled,
    /// otherwise returns `None`.
    pub fn get_shader(&self) -> Option<&Program> {
        self.compiled_shader.as_ref()
    }
}
