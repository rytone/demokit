use std::io;
use std::path::Path;

use notify;

use self::watcher::{ReloadError, ReloadResult, ReloadWatcher};

/// A hot-reloadable asset.
pub trait Asset<D> {
    /// Creates an asset from raw data.
    ///
    /// # Arguments
    ///
    /// * `data` - The data to construct the asset with.
    fn from_data(data: D) -> Self;

    /// Load data from a file into the asset's own data type.
    ///
    /// # Arguments
    ///
    /// * `path` - The file path to load the data from relative to the current
    /// working directory.
    fn load_data<P: AsRef<Path>>(path: P) -> io::Result<D>;

    /// Creates an asset from data stored in a file.
    ///
    /// # Arguments
    ///
    /// * `path` - The file path to load the data from relative to the current
    /// working directory.
    fn from_file<P: AsRef<Path>>(path: P) -> io::Result<Self>
    where
        Self: Sized,
    {
        Ok(Self::from_data(Self::load_data(path)?))
    }

    /// Returns a borrow to the asset's inner data.
    fn get_data(&self) -> &D;

    /// Sets the asset's data.
    fn set_data(&mut self, data: D);

    /// Constructs a `ReloadWatcher` on the asset.
    ///
    /// # Arguments
    ///
    /// * `path` - The file path to watch for changes relative to the current
    /// working directory.
    fn watch(&mut self, path: &str) -> notify::Result<()>;

    /// Returns a borrow to the bound `ReloadWatcher` if the asset is being watched.
    fn watcher(&self) -> Option<&ReloadWatcher>;

    /// Polls the asset's internal `ReloadWatcher` for changes.
    fn hot_reload(&mut self) -> Result<ReloadResult, ReloadError> {
        let path = self.watcher().ok_or(ReloadError::AssetNotWatched)?.poll();
        if let Some(path) = path {
            self.set_data(Self::load_data(path)?);
            return Ok(ReloadResult::Reload);
        }
        Ok(ReloadResult::NoReload)
    }
}

pub mod watcher;

pub mod audio;
pub use self::audio::AudioAsset;

pub mod data;
pub use self::data::DataAsset;

pub mod obj;
pub use self::obj::ObjAsset;

pub mod image;
pub use self::image::ImageAsset;

pub mod string;
pub use self::string::StringAsset;

pub mod shader;
pub use self::shader::ShaderAsset;

pub mod reloader;
pub use self::reloader::Reloader;

pub mod macros;
