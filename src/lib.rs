extern crate ansi_term;
extern crate chrono;
extern crate clap;
#[macro_use]
extern crate glium;
extern crate image;
#[macro_use]
extern crate log;
extern crate notify;
extern crate obj;
extern crate rodio;
#[macro_use]
extern crate quick_error;

pub mod assets;

pub mod demo;
pub mod util;

mod music;

pub mod pipeline;
