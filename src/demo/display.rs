use std::num::ParseIntError;
use std::str::FromStr;

use glium::glutin::{dpi::PhysicalSize, MonitorId};

#[derive(Copy, Clone)]
pub enum DisplayResolution {
    Native,
    Custom { x: u32, y: u32 },
}

quick_error! {
    #[derive(Debug)]
    pub enum ResolutionParseError {
        IntParse(err: ParseIntError) {
            cause(err)
            description("The passed resolution is not an integer value.")
            from()
        }
        InvalidFormat {
            description("The passed resolution is formatted incorrectly.")
        }
    }
}

impl FromStr for DisplayResolution {
    type Err = ResolutionParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_ref() {
            "native" => Ok(DisplayResolution::Native),
            s => {
                let vals: Result<Vec<u32>, ParseIntError> =
                    s.splitn(2, 'x').map(FromStr::from_str).collect();
                let vals = vals?;
                Ok(DisplayResolution::Custom {
                    x: *(vals.get(0).ok_or(ResolutionParseError::InvalidFormat)?),
                    y: *(vals.get(1).ok_or(ResolutionParseError::InvalidFormat)?),
                })
            }
        }
    }
}

impl DisplayResolution {
    /// Returns the exact dimensions of the `DisplayResolution`.
    ///
    /// # Arguments
    ///
    /// * `monitor` - `MonitorId` of the monitor to get the native resolution from.
    pub fn true_resolution(&self, monitor: &MonitorId) -> PhysicalSize {
        match *self {
            DisplayResolution::Native => monitor.get_dimensions(),
            DisplayResolution::Custom { x, y } => PhysicalSize::from((x, y)),
        }
    }
}

pub struct DisplayParameters {
    pub resolution: DisplayResolution,
    pub fullscreen: bool,
    pub msaa: u16,
    pub vsync: bool,
}

impl Default for DisplayParameters {
    fn default() -> Self {
        DisplayParameters {
            resolution: DisplayResolution::Native,
            fullscreen: true,
            msaa: 0,
            vsync: true,
        }
    }
}
