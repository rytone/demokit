use glium;
use rodio::decoder::DecoderError;

use assets::{AudioAsset, Reloader};
use demo::display::DisplayParameters;
use pipeline::{Pipeline, Stage};

/// Stores the current time in the demo in both seconds and beats.
pub struct DemoTime {
    pub secs: f64,
    pub beats: f64,
}

pub struct DemoContext {
    pub gl: glium::Display,
    pub frame: Option<glium::Frame>,
    pub time: DemoTime,
    pub reloader: Reloader,
    pub target_res: (u32, u32),
}

quick_error! {
    #[derive(Debug)]
    pub enum DemoError {
        ResolutionError(err: display::ResolutionParseError) {
            cause(err)
            description(err.description())
            from()
        }
        MSAAError {
            description("The passed MSAA value is not an integer")
        }
        MonitorIdError {
            description("The passed monitor does not exist")
        }
        ContextError(err: glium::backend::glutin::DisplayCreationError) {
            cause(err)
            description(err.description())
            display("Failed to create the OpenGL context: {}", err)
            from()
        }
        NoAudioDevices {
            description("No audio devices found.")
        }
        MusicDecodeError(err: DecoderError) {
            cause(err)
            description(err.description())
            display("Music failed to decode: {}", err)
            from()
        }
    }
}

/// All of the data needed to create a struct implementing `Demo`
pub struct DemoParams {
    title: &'static str,
    author: &'static str,

    music_bpm: f32,
    display_defaults: DisplayParameters,
}

pub struct Demo<
    S: Stage<Input = (), Output = ()>,
    F: FnOnce(&mut DemoContext) -> S,
    B: DemoBackend<S, F>,
> {
    backend: B,
    params: DemoParams,
    pipeline: Pipeline<S, F>,
    music: Option<AudioAsset>,
}

impl<S: Stage<Input = (), Output = ()>, F: FnOnce(&mut DemoContext) -> S, B: DemoBackend<S, F>>
    Demo<S, F, B>
{
    fn new(
        backend: B,
        params: DemoParams,
        pipeline: Pipeline<S, F>,
        music: Option<AudioAsset>,
    ) -> Self {
        Demo {
            backend,
            params,
            pipeline,
            music,
        }
    }

    pub fn run(self) -> Result<(), DemoError> {
        self.backend.run(self.params, self.pipeline, self.music)
    }
}

pub mod backend;
use self::backend::DemoBackend;
pub use self::backend::ReleaseBackend;

pub mod builder;
pub use self::builder::DemoBuilder;

pub mod display;
