use assets::AudioAsset;
use demo::backend::DemoBackend;
use demo::display::DisplayParameters;
use demo::{Demo, DemoContext, DemoParams};
use pipeline::{Pipeline, Stage};

/// Builder used to construct structs implementing `Demo`
pub struct DemoBuilder<S: Stage<Input = (), Output = ()>, F: FnOnce(&mut DemoContext) -> S> {
    title: Option<&'static str>,
    author: Option<&'static str>,
    default_display: Option<DisplayParameters>,
    pipeline: Pipeline<S, F>,
    music_asset: Option<AudioAsset>,
    music_bpm: Option<f32>,
}

impl<S: Stage<Input = (), Output = ()>, F: FnOnce(&mut DemoContext) -> S> DemoBuilder<S, F> {
    pub fn from_pipeline(pipeline: Pipeline<S, F>) -> Self {
        DemoBuilder {
            pipeline,

            title: None,
            author: None,
            default_display: None,
            music_asset: None,
            music_bpm: None,
        }
    }

    /// Sets the title of the demo
    pub fn title(mut self, title: &'static str) -> Self {
        self.title = Some(title);
        self
    }

    /// Sets the author of the demo
    pub fn author(mut self, author: &'static str) -> Self {
        self.author = Some(author);
        self
    }

    /// Overiddes the default display settings for the demo. By default,
    /// demos will run in fullscreen at the monitor's native resolution.
    pub fn default_display(mut self, display: DisplayParameters) -> Self {
        self.default_display = Some(display);
        self
    }

    /// Sets the music for the demo.
    ///
    /// # Note
    ///
    /// This is optional, if music is not provided, the demo will run silently forever.
    pub fn music(mut self, asset: AudioAsset) -> Self {
        self.music_asset = Some(asset);
        self
    }

    /// Sets the beat rate for the demo's music.
    ///
    /// # Note
    ///
    /// Unline `DemoBuilder::music`, this method is required in order for the
    /// demo to be successfully built.
    pub fn bpm(mut self, bpm: f32) -> Self {
        self.music_bpm = Some(bpm);
        self
    }

    pub fn with_backend<B: DemoBackend<S, F>>(self, backend: B) -> Demo<S, F, B> {
        let params = DemoParams {
            author: self.author.expect("author required"),
            title: self.title.expect("title required"),

            music_bpm: self.music_bpm.unwrap_or(60.0),
            display_defaults: self.default_display
                .unwrap_or_else(|| DisplayParameters::default()),
        };
        Demo::new(backend, params, self.pipeline, self.music_asset)
    }
}
