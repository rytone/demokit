use std::io::Cursor;

use ansi_term::Style;

use glium;
use glium::glutin;

use rodio::default_output_device;

use assets::{AudioAsset, Reloader};
use demo::backend::DemoBackend;
use demo::{DemoContext, DemoError, DemoParams, DemoTime};
use music::control::{ControlHandle, SeekMode};
use music::{Decode, MusicController, MusicTime};
use pipeline::profiler::ProfilableStage;
use pipeline::Stage;

pub struct DebugBackend;
impl<S: Stage<Input = (), Output = ()>, F: FnOnce(&mut DemoContext) -> S> DemoBackend<S, F>
    for DebugBackend
where
    S: ProfilableStage,
{
    fn start_music(&self, asset: Option<AudioAsset>) -> Result<ControlHandle, DemoError> {
        let device = default_output_device().ok_or(DemoError::NoAudioDevices)?;

        let ctl: MusicController<Cursor<Vec<u8>>> = if let Some(a) = asset {
            MusicController::Decoded(a.into_source()?.decode())
        } else {
            MusicController::None
        };

        Ok(ctl.play(&device))
    }

    fn initial_context(&self, display: glium::Display, target_res: (u32, u32)) -> DemoContext {
        DemoContext {
            gl: display,
            frame: None,
            time: DemoTime {
                secs: -1.0,
                beats: -1.0,
            },
            reloader: Reloader::Default,
            target_res,
        }
    }

    fn run_event_loop(
        &self,
        mut ev_loop: glutin::EventsLoop,
        mut stage: S,
        mut ctx: DemoContext,
        params: DemoParams,
        mut music_ctl: ControlHandle,
    ) {
        let mut done = false;
        while !done {
            let time = music_ctl.elapsed();
            ctx.frame = Some(ctx.gl.draw());
            ctx.time = DemoTime {
                secs: time.to_secs(),
                beats: time.to_beats(params.music_bpm),
            };

            let (_, profile) = stage.profiled_exec(&mut ctx, ());
            if let Some(frame) = ctx.frame.take() {
                warn!("frame left unfinished! swapping buffers anyway");
                frame.finish().unwrap();
            }

            let frametime = profile.time();
            let fps = 1000.0 / frametime;

            ev_loop.poll_events(|e| {
                if let glutin::Event::WindowEvent { event, .. } = e {
                    match event {
                        glutin::WindowEvent::CloseRequested => done = true,
                        glutin::WindowEvent::KeyboardInput { input, .. } => if input.state
                            == glutin::ElementState::Pressed
                        // rustfmt plz
                        {
                            match input.virtual_keycode {
                                Some(glutin::VirtualKeyCode::Escape) => done = true,

                                Some(glutin::VirtualKeyCode::Space) => music_ctl.toggle_pause(),
                                Some(glutin::VirtualKeyCode::G) => music_ctl.seek(SeekMode::Start),
                                Some(glutin::VirtualKeyCode::H) => {
                                    music_ctl.seek(SeekMode::Backward(1.0))
                                }
                                Some(glutin::VirtualKeyCode::J) => {
                                    music_ctl.seek(SeekMode::Backward(0.01))
                                }
                                Some(glutin::VirtualKeyCode::K) => {
                                    music_ctl.seek(SeekMode::Forward(0.01))
                                }
                                Some(glutin::VirtualKeyCode::L) => {
                                    music_ctl.seek(SeekMode::Forward(1.0))
                                }

                                Some(glutin::VirtualKeyCode::P) => {
                                    println!(
                                        "\n{stat_msg}\n{div}\ntime = {beats:.2} beats ({time:.2}s)\nft = {ft:.2}ms ({fps:.2}fps)\n\n{profile}\n",
                                        stat_msg = Style::new().bold().paint("performance stats"),
                                        div = "-------------------------------------",
                                        beats = time.to_beats(params.music_bpm),
                                        time = time.to_secs(),
                                        ft = frametime,
                                        fps = fps,
                                        profile = profile,
                                    );
                                }
                                Some(glutin::VirtualKeyCode::X) => debug!("[todo] help"),

                                _ => (),
                            }
                        },
                        _ => (),
                    }
                }
            });

            if music_ctl.done() {
                done = true;
            }
        }
    }
}
