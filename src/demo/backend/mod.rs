use std::process;

use clap;

use glium;
use glium::glutin;

use rodio::default_output_device;

use assets::{AudioAsset, Reloader};
use demo::display::{DisplayParameters, DisplayResolution};
use demo::{DemoContext, DemoError, DemoParams, DemoTime};
use music::control::ControlHandle;
use music::{MusicController, MusicTime};
use pipeline::{Pipeline, Stage};

pub trait DemoBackend<S: Stage<Input = (), Output = ()>, F: FnOnce(&mut DemoContext) -> S> {
    fn get_args<'a, 'b>(&'a self, params: &'a DemoParams) -> clap::ArgMatches<'b> {
        clap::App::new(params.title.clone())
            .author(params.author.clone())
            .arg(clap::Arg::with_name("fullscreen")
                 .short("f")
                 .long("fullscreen")
                 .help("force the demo to run fullscreen")
            )
            .arg(clap::Arg::with_name("windowed")
                .short("w")
                .long("windowed")
                .help("force the demo to run windowed")
            )
            .arg(clap::Arg::with_name("resolution")
                 .long("resolution")
                 .takes_value(true)
                 .value_name("res")
                 .help("force the demo to run at a certain resolution\ncan be either \"[width]x[height]\" or \"native\"")
            )
            .arg(clap::Arg::with_name("msaa")
                .long("msaa")
                .takes_value(true)
                .value_name("samples")
                .help("set the msaa level")
            )
            .arg(clap::Arg::with_name("vsync_off")
                .short("v")
                .long("disable-vsync")
                .help("force vsync to be disabled")
            )
            .arg(clap::Arg::with_name("vsync_on")
                .short("V")
                .long("enable-vsync")
                .help("force vsync to be enabled")
            )
            .arg(clap::Arg::with_name("list_monitors")
                .long("list-monitors")
                .help("print the monitors available for use with --monitor")
            )
            .arg(clap::Arg::with_name("monitor")
                .long("monitor")
                .takes_value(true)
                .value_name("number")
                .help("run the demo on a specific monitor")
            )
            .get_matches()
    }

    fn create_display(
        &self,
        params: DisplayParameters,
        title: &str,
        monitor_id: Option<usize>,
    ) -> Result<(glutin::EventsLoop, glium::Display, (u32, u32)), DemoError> {
        let ev_loop = glutin::EventsLoop::new();
        let monitor = if let Some(id) = monitor_id {
            ev_loop
                .get_available_monitors()
                .nth(id)
                .ok_or(DemoError::MonitorIdError)
        } else {
            Ok(ev_loop.get_primary_monitor())
        }?;
        let res = params.resolution.true_resolution(&monitor);
        let win_builder = glutin::WindowBuilder::new()
            .with_resizable(false)
            .with_title(title);
        let win_builder = if params.fullscreen {
            win_builder.with_fullscreen(Some(monitor))
        } else {
            win_builder.with_dimensions(res.to_logical(monitor.get_hidpi_factor()))
        };

        let ctx_builder = glutin::ContextBuilder::new()
            .with_vsync(params.vsync)
            .with_srgb(true);

        let display = glium::Display::new(win_builder, ctx_builder, &ev_loop)?;
        display.gl_window().hide_cursor(true);

        Ok((ev_loop, display, res.into()))
    }

    fn initial_context(&self, display: glium::Display, target_res: (u32, u32)) -> DemoContext {
        DemoContext {
            gl: display,
            frame: None,
            time: DemoTime {
                secs: -1.0,
                beats: -1.0,
            },
            reloader: Reloader::None,
            target_res,
        }
    }

    fn start_music(&self, asset: Option<AudioAsset>) -> Result<ControlHandle, DemoError> {
        let device = default_output_device().ok_or(DemoError::NoAudioDevices)?;

        let ctl = if let Some(a) = asset {
            MusicController::Decoder(a.into_source()?)
        } else {
            MusicController::None
        };

        Ok(ctl.play(&device))
    }

    fn run_event_loop(
        &self,
        mut ev_loop: glutin::EventsLoop,
        mut stage: S,
        mut ctx: DemoContext,
        params: DemoParams,
        music_ctl: ControlHandle,
    ) {
        let mut done = false;
        while !done {
            ev_loop.poll_events(|e| {
                if let glutin::Event::WindowEvent { event, .. } = e {
                    match event {
                        glutin::WindowEvent::CloseRequested => done = true,
                        glutin::WindowEvent::KeyboardInput { input, .. } => {
                            if input.virtual_keycode == Some(glutin::VirtualKeyCode::Escape) {
                                done = true
                            }
                        }
                        _ => (),
                    }
                }
            });

            let time = music_ctl.elapsed();
            ctx.frame = Some(ctx.gl.draw());
            ctx.time = DemoTime {
                secs: time.to_secs(),
                beats: time.to_beats(params.music_bpm),
            };

            stage.exec(&mut ctx, ());
            if let Some(frame) = ctx.frame.take() {
                warn!("frame left unfinished! swapping buffers anyway");
                frame.finish().unwrap();
            }

            if music_ctl.done() {
                done = true;
            }
        }
    }

    fn run(
        &self,
        params: DemoParams,
        pipeline: Pipeline<S, F>,
        music: Option<AudioAsset>,
    ) -> Result<(), DemoError> {
        #[cfg(windows)]
        #[allow(unused)]
        {
            use ansi_term;
            // may fail if the logging implementation already handles this
            ansi_term::enable_ansi_support();
        };
        info!("loading...");

        let args = self.get_args(&params);

        let res = args.value_of("resolution")
            .map(|s| s.parse::<DisplayResolution>())
            .unwrap_or_else(|| Ok(params.display_defaults.resolution))?;
        let fs = if args.is_present("fullscreen") | args.is_present("windowed") {
            args.is_present("fullscreen")
        } else {
            params.display_defaults.fullscreen
        };
        let msaa = if let Some(arg_msaa) = args.value_of("msaa") {
            arg_msaa.parse::<u16>().map_err(|_| DemoError::MSAAError)?
        } else {
            params.display_defaults.msaa
        };
        let vs = if args.is_present("vsync_on") {
            true
        } else if args.is_present("vsync_off") {
            false
        } else {
            params.display_defaults.vsync
        };
        if args.is_present("list_monitors") {
            println!("Available monitors:");
            let ev_loop = glutin::EventsLoop::new();
            let monitors = ev_loop.get_available_monitors();
            for (i, monitor) in monitors.enumerate() {
                println!("[{}] {}", i, monitor.get_name().unwrap_or_else(String::new));
            }
            process::exit(0);
        }
        let monitor_id = if let Some(id) = args.value_of("monitor") {
            id.parse::<usize>()
                .map(Some)
                .map_err(|_| DemoError::MonitorIdError)
        } else {
            Ok(None)
        }?;

        debug!("creating display...");
        let (ev_loop, display, target_res) = self.create_display(
            DisplayParameters {
                resolution: res,
                fullscreen: fs,
                msaa,
                vsync: vs,
            },
            &format!("{} - {}", params.author, params.title),
            monitor_id,
        )?;

        // IMPORTANT STUFF MOVED INTO CONTEXT FROM HERE FORWARD
        let mut ctx = self.initial_context(display, target_res);

        debug!("loading pipeline...");
        ctx.frame = Some(ctx.gl.draw());
        let stage = pipeline.load(&mut ctx);
        if let Some(frame) = ctx.frame.take() {
            warn!("frame left unfinished! swapping buffers anyway");
            frame.finish().unwrap();
        }

        debug!("loading music...");
        let music_ctl = self.start_music(music)?;

        info!("done");
        self.run_event_loop(ev_loop, stage, ctx, params, music_ctl);

        Ok(())
    }
}

pub mod debug;
pub use self::debug::DebugBackend;
pub mod release;
pub use self::release::ReleaseBackend;
