use demo::DemoContext;
use demo::backend::DemoBackend;
use pipeline::Stage;

pub struct ReleaseBackend;
impl<S: Stage<Input = (), Output = ()>, F: FnOnce(&mut DemoContext) -> S> DemoBackend<S, F>
    for ReleaseBackend
{
}
