pub mod decoded;
pub use self::decoded::Decode;

pub mod control;
pub use self::control::MusicController;

pub mod time;
pub use self::time::MusicTime;

mod nop;
