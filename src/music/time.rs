use std::time::Duration;

/// Converts the implementing type into either seconds or beats.
pub trait MusicTime {
    /// Converts the value into seconds.
    fn to_secs(&self) -> f64;
    /// Converts the value into beats.
    fn to_beats(&self, bpm: f32) -> f64 {
        self.to_secs() * (f64::from(bpm) / 60.0)
    }
}

impl MusicTime for Duration {
    fn to_secs(&self) -> f64 {
        let whole_secs = self.as_secs() as f64;
        let sub_secs = f64::from(self.subsec_nanos());

        whole_secs + sub_secs / 1e9
    }
}
