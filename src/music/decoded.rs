use std::io::{Read, Seek};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::time::Duration;

use rodio::{Decoder, Source};

use music::control::seek::{MusicSeek, SeekMode};

/// An audio source that has been pre-decoded
pub struct DecodedSource {
    channels: u16,
    sample_rate: u32,
    total_duration: Option<Duration>,

    position: usize,
    decoded_samples: Vec<i16>,

    seek_tx: Sender<SeekMode>,
    seek_rx: Receiver<SeekMode>,
}

impl Iterator for DecodedSource {
    type Item = i16;

    fn next(&mut self) -> Option<Self::Item> {
        if let Ok(seek) = self.seek_rx.try_recv() {
            self.seek(seek);
        }
        self.position += 1;
        self.decoded_samples.get(self.position).cloned()
    }
}

impl Source for DecodedSource {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn sample_rate(&self) -> u32 {
        self.sample_rate
    }

    fn total_duration(&self) -> Option<Duration> {
        self.total_duration
    }
}

impl DecodedSource {
    /// Decodes a `rodio::Decoder` into a `DecodedSource`
    pub fn new<T: Read + Seek>(source: Decoder<T>) -> Self {
        let (seek_tx, seek_rx) = channel();
        DecodedSource {
            channels: source.channels(),
            sample_rate: source.sample_rate(),
            total_duration: source.total_duration(),

            position: 0,
            decoded_samples: source.collect(),

            seek_tx,
            seek_rx,
        }
    }
}

/// Trait to allow decoding of any `rodio::Source`.
pub trait Decode {
    fn decode(self) -> DecodedSource;
}

impl<T: Read + Seek> Decode for Decoder<T> {
    fn decode(self) -> DecodedSource {
        DecodedSource::new(self)
    }
}

impl MusicSeek for DecodedSource {
    fn seek(&mut self, mode: SeekMode) {
        let sr = f64::from(self.sample_rate);
        match mode {
            SeekMode::Forward(amt) => {
                let s_move = (amt * sr * 2.0).round() as usize;
                self.position = (self.position + s_move).min(self.decoded_samples.len());
            }
            SeekMode::Backward(amt) => {
                let s_move = (amt * sr * 2.0).round() as usize;
                self.position = self.position
                    .saturating_sub(s_move)
                    .min(self.decoded_samples.len());
            }
            SeekMode::Start => self.position = 0,
        }
    }

    fn seek_handle(&self) -> Sender<SeekMode> {
        self.seek_tx.clone()
    }
}
