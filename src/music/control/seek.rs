use std::sync::mpsc::Sender;

/// The 3 different ways a music source can seek.
pub enum SeekMode {
    /// Seeks the source forwards by a specified number of seconds.
    Forward(f64),
    /// Seeks the source backwards by a specified number of seconds.
    Backward(f64),
    /// Restarts the source.
    Start,
}

/// Allows audio sources to seek forwards, backwards, and restart.
pub trait MusicSeek {
    /// Seeks the source by the specified mode.
    fn seek(&mut self, mode: SeekMode);
    /// Returns a `sync::mpsc::Sender` that seek events can be pushed to.
    fn seek_handle(&self) -> Sender<SeekMode>;
}
