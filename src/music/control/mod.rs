use std::io::{Read, Seek};
use std::sync::mpsc::Sender;
use std::time::{Duration, Instant};

use rodio::{Decoder, Device, Sink};

use music::decoded::DecodedSource;
use music::nop::NopSource;

/// A controller for audio that has been submitted to an audio sink.
/// This struct holds ownership of said sink.
pub struct ControlHandle {
    sink: Sink,

    start_time: Instant,
    paused_time: Option<Instant>,
    paused_location: Option<Duration>,

    seek_handle: Option<Sender<self::seek::SeekMode>>,
}

impl ControlHandle {
    /// Constructs a new `ControlHandle`
    ///
    /// # Arguments
    ///
    /// `sink` - The audio sink containing the audio.
    /// `seek_handle` - An optional `sync::mpsc::Sender` where seek events can be sent.
    fn new(sink: Sink, seek_handle: Option<Sender<self::seek::SeekMode>>) -> Self {
        ControlHandle {
            sink,
            start_time: Instant::now(),
            paused_time: None,
            paused_location: None,
            seek_handle,
        }
    }

    /// Pauses the audio.
    pub fn pause(&mut self) {
        self.paused_time = Some(Instant::now());
        self.paused_location = Some(self.start_time.elapsed());
        self.sink.pause();
    }

    /// Resumes the audio.
    pub fn play(&mut self) {
        self.start_time += self.paused_time
            .map(|t| t.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if self.start_time > Instant::now() {
            self.start_time = Instant::now();
        }
        self.paused_time = None;
        self.paused_location = None;
        self.sink.play();
    }

    /// Toggles the audio's play/pause status.
    pub fn toggle_pause(&mut self) {
        if self.sink.is_paused() {
            self.play();
        } else {
            self.pause();
        }
    }

    /// Returns the current time elapsed through the audio.
    pub fn elapsed(&self) -> Duration {
        self.paused_location
            .unwrap_or_else(|| self.start_time.elapsed())
    }

    /// Returns true if the audio has completed playback.
    pub fn done(&self) -> bool {
        self.sink.empty()
    }

    /// Sends a seek event to the audio's seek handle if it exists.
    ///
    /// # Arguments
    ///
    /// * `mode` - The seek event
    ///
    /// # Note
    ///
    /// Even if there is not a seek handle present, the elapsed time will still
    /// seek around, independent of the audio.
    pub fn seek(&mut self, mode: self::seek::SeekMode) {
        match mode {
            self::seek::SeekMode::Forward(amt) => {
                let seek_duration = Duration::from_millis((amt * 1000.0).round() as u64);
                self.start_time -= seek_duration;
                self.paused_location = self.paused_location.map(|t| t + seek_duration);
            }
            self::seek::SeekMode::Backward(amt) => {
                let seek_duration = Duration::from_millis((amt * 1000.0).round() as u64);
                self.start_time += seek_duration;
                if self.start_time > Instant::now() {
                    self.start_time = Instant::now();
                }
                self.paused_location = self.paused_location.map(|t| {
                    t.checked_sub(seek_duration)
                        .unwrap_or_else(|| Duration::from_secs(0))
                });
            }
            self::seek::SeekMode::Start => {
                self.start_time = Instant::now();
                if self.paused_time.is_some() {
                    self.paused_time = Some(self.start_time);
                    self.paused_location = Some(Duration::from_secs(0));
                }
            }
        }
        if let Some(ref handle) = self.seek_handle {
            handle.send(mode).unwrap();
        }
    }
}

/// Wraps different types of `rodio::Source`s to provide a uniform way of
/// playing back audio and creating ControlHandles.
pub enum MusicController<T: Read + Seek + Send + 'static> {
    /// A `rodio::Decoder`, will not create a channel to send seek events on.
    Decoder(Decoder<T>),
    /// A pre-decoded source, will create a channel to send seek events on.
    Decoded(DecodedSource),
    /// A source that infinitely provides silent audio samples.
    None,
}

impl<T: Read + Seek + Send + 'static> MusicController<T> {
    /// Plays back the source stored in the `MusicController`, consuming it and
    /// returning a `ControlHandle`.
    pub fn play(self, device: &Device) -> ControlHandle {
        let sink = Sink::new(device);
        let seek_handle = match self {
            MusicController::Decoder(s) => {
                sink.append(s);
                None
            }
            MusicController::Decoded(s) => {
                let handle = s.seek_handle();
                sink.append(s);
                Some(handle)
            }
            MusicController::None => {
                sink.append(NopSource::default());
                None
            }
        };
        ControlHandle::new(sink, seek_handle)
    }
}

pub mod seek;
pub use self::seek::{MusicSeek, SeekMode};
