use rodio::Source;
use std::time::Duration;

#[derive(Default)]
/// An audio source that infinitely sends silent samples.
pub struct NopSource;

impl Iterator for NopSource {
    type Item = i16;
    fn next(&mut self) -> Option<Self::Item> {
        Some(0)
    }
}

impl Source for NopSource {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        44_100
    }

    fn total_duration(&self) -> Option<Duration> {
        None
    }
}
