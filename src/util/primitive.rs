use glium::backend::Facade;
use glium::index::PrimitiveType;
use glium::{IndexBuffer, VertexBuffer};

#[derive(Copy, Clone)]
pub struct PrimitiveVertex {
    position: [f32; 3],
}
implement_vertex!(PrimitiveVertex, position);

pub fn quad<F: Facade>(facade: &F) -> (VertexBuffer<PrimitiveVertex>, IndexBuffer<u32>) {
    let vbuf = VertexBuffer::new(
        facade,
        &[
            PrimitiveVertex {
                position: [-1.0, 1.0, 0.0],
            },
            PrimitiveVertex {
                position: [1.0, 1.0, 0.0],
            },
            PrimitiveVertex {
                position: [-1.0, -1.0, 0.0],
            },
            PrimitiveVertex {
                position: [1.0, -1.0, 0.0],
            },
        ],
    ).unwrap();
    let ibuf = IndexBuffer::new(facade, PrimitiveType::TrianglesList, &[0, 1, 2, 1, 2, 3]).unwrap();
    (vbuf, ibuf)
}
