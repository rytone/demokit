use std::fmt;
use std::rc::Rc;

use glium::texture::Texture2d;
use glium::uniforms::MagnifySamplerFilter;
use glium::{BlitTarget, Rect, Surface};

use demo::DemoContext;
use pipeline::Stage;

#[derive(Debug)]
pub struct ClearDisplay;

impl ClearDisplay {
    pub fn new(_: &mut DemoContext) -> Self {
        ClearDisplay
    }
}

impl fmt::Display for ClearDisplay {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ClearDisplay")
    }
}

impl Stage for ClearDisplay {
    type Input = ();
    type Output = ();

    fn exec(&mut self, ctx: &mut DemoContext, _: Self::Input) -> Self::Output {
        ctx.frame
            .as_mut()
            .unwrap()
            .clear_color_and_depth((0.0, 0.0, 0.0, 1.0), 1.0);
    }
}

#[derive(Debug)]
/// Pipeline stage for completing OpenGL draw calls and swapping the display buffers.
pub struct FinishDisplay;

impl FinishDisplay {
    pub fn new(ctx: &mut DemoContext) -> Self {
        if let Some(frame) = ctx.frame.take() {
            frame.finish().unwrap();
        }
        FinishDisplay
    }
}

impl fmt::Display for FinishDisplay {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "FinishDisplay")
    }
}

impl Stage for FinishDisplay {
    type Input = ();
    type Output = ();

    fn exec(&mut self, ctx: &mut DemoContext, _: Self::Input) {
        ctx.frame.take().unwrap().finish().unwrap();
    }
}

#[derive(Debug)]
pub struct BlitTexture {
    scale_to_target: bool,
}

impl BlitTexture {
    pub fn default(_: &mut DemoContext) -> Self {
        BlitTexture {
            scale_to_target: true,
        }
    }

    pub fn retain_source_size(_: &mut DemoContext) -> Self {
        BlitTexture {
            scale_to_target: false,
        }
    }
}

impl fmt::Display for BlitTexture {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "BlitTexture")
    }
}

impl Stage for BlitTexture {
    type Input = Rc<Texture2d>;
    type Output = ();

    fn exec(&mut self, ctx: &mut DemoContext, input: Self::Input) {
        let source = input.as_surface();
        let target = ctx.frame.as_ref().unwrap();

        let (source_width, source_height) = source.get_dimensions();
        let source_rect = Rect {
            left: 0,
            bottom: 0,
            width: source_width,
            height: source_height,
        };

        let blit_target = if self.scale_to_target {
            let (target_width, target_height) = target.get_dimensions();
            BlitTarget {
                left: 0,
                bottom: 0,
                width: target_width as i32,
                height: target_height as i32,
            }
        } else {
            BlitTarget {
                left: 0,
                bottom: 0,
                width: source_width as i32,
                height: source_height as i32,
            }
        };

        source.blit_color(
            &source_rect,
            target,
            &blit_target,
            MagnifySamplerFilter::Linear,
        );
    }
}
