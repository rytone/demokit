use demo::DemoContext;

pub trait Stage {
    type Input;
    type Output;

    fn exec(&mut self, ctx: &mut DemoContext, input: Self::Input) -> Self::Output;
}

pub struct CombinedStage<T, U>
where
    T: Stage,
    U: Stage<Input = T::Output>,
{
    this: T,
    next: U,
}

impl<T, U> CombinedStage<T, U>
where
    T: Stage,
    U: Stage<Input = T::Output>,
{
    fn new(this: T, next: U) -> Self {
        CombinedStage { this, next }
    }
}

impl<T, U> Stage for CombinedStage<T, U>
where
    T: Stage,
    U: Stage<Input = T::Output>,
{
    type Input = T::Input;
    type Output = U::Output;

    fn exec(&mut self, ctx: &mut DemoContext, input: T::Input) -> U::Output {
        let out = self.this.exec(ctx, input);
        self.next.exec(ctx, out)
    }
}

pub struct SubStage<T: Stage> {
    name: String,
    stage: T,
}

impl<T: Stage> SubStage<T> {
    pub fn new<S: Into<String>>(name: S, stage: T) -> Self {
        SubStage {
            name: name.into(),
            stage,
        }
    }
}

impl<T: Stage> Stage for SubStage<T> {
    type Input = T::Input;
    type Output = T::Output;

    fn exec(&mut self, ctx: &mut DemoContext, input: T::Input) -> T::Output {
        self.stage.exec(ctx, input)
    }
}

pub struct Pipeline<T: Stage, F: FnOnce(&mut DemoContext) -> T> {
    initializer: F,
}

impl<T: Stage, F: FnOnce(&mut DemoContext) -> T> Pipeline<T, F> {
    pub fn new(initializer: F) -> Self {
        Pipeline { initializer }
    }

    pub fn then<S, N>(
        self,
        next_init: N,
    ) -> Pipeline<CombinedStage<T, S>, impl FnOnce(&mut DemoContext) -> CombinedStage<T, S>>
    where
        S: Stage<Input = T::Output>,
        N: FnOnce(&mut DemoContext) -> S,
    {
        Pipeline {
            initializer: move |ctx: &mut DemoContext| {
                let this = (self.initializer)(ctx);
                let next = (next_init)(ctx);
                CombinedStage::new(this, next)
            },
        }
    }

    pub fn named<S: Into<String>>(self, name: S) -> impl FnOnce(&mut DemoContext) -> SubStage<T> {
        move |ctx: &mut DemoContext| SubStage::new(name, (self.initializer)(ctx))
    }

    pub fn load(self, ctx: &mut DemoContext) -> T {
        (self.initializer)(ctx)
    }
}

pub mod profiler;
