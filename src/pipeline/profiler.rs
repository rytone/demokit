use ansi_term::Color;
use std::fmt;
use std::time::Instant;

use demo::DemoContext;
use pipeline::{CombinedStage, Stage, SubStage};

const LOW_IMPACT_MAX: f64 = 3.0;
const MED_IMPACT_MAX: f64 = 6.0;
const HIGH_IMPACT_MAX: f64 = 9.0;

#[derive(Debug)]
/// A tree-like repersentation of profiling data returned from `Pipeline::profiled_exec`
pub enum ProfileTree {
    MultiRoot(Vec<ProfileTree>),
    Stage {
        name: String,
        time: f64,
    },
    MultiStage {
        name: String,
        stages: Vec<ProfileTree>,
    },
}

impl ProfileTree {
    fn into_vec(self) -> Vec<ProfileTree> {
        match self {
            ProfileTree::MultiRoot(v) => v,
            _ => vec![self],
        }
    }

    /// Merges 2 `ProfileTree`s into a single `ProfileTree::MultiRoot`.
    /// If one or both of the trees are already multiroots, then it will
    /// properly merge it without having nested multiroots.
    pub fn merge(self, b: ProfileTree) -> ProfileTree {
        let mut a_vec = self.into_vec();
        let mut b_vec = b.into_vec();

        a_vec.append(&mut b_vec);
        ProfileTree::MultiRoot(a_vec)
    }

    /// Calculates the sum time of all of the entire `ProfileTree`
    pub fn time(&self) -> f64 {
        match *self {
            ProfileTree::MultiRoot(ref v) => v.iter().map(|s| s.time()).sum(),
            ProfileTree::Stage { time, .. } => time,
            ProfileTree::MultiStage { ref stages, .. } => stages.iter().map(|s| s.time()).sum(),
        }
    }

    fn format(&self, indent: &str, prefix: &str, is_last: bool) -> String {
        match *self {
            ProfileTree::MultiRoot(ref stages) => {
                let len = stages.len();
                stages
                    .iter()
                    .enumerate()
                    .map(|(i, stage)| stage.format(indent, get_prefix(i, len, true), i + 1 == len))
                    .collect::<Vec<String>>()
                    .join("\n")
            }
            ProfileTree::Stage { ref name, ref time } => {
                let color = if *time <= LOW_IMPACT_MAX {
                    Color::Green
                } else if *time <= MED_IMPACT_MAX {
                    Color::Yellow
                } else if *time <= HIGH_IMPACT_MAX {
                    Color::Red
                } else {
                    Color::Purple
                };
                format!(
                    "{}",
                    color.paint(format!("{}{}{} ({:.3}ms)", indent, prefix, name, time))
                )
            }
            ProfileTree::MultiStage {
                ref name,
                ref stages,
            } => {
                let len = stages.len();
                let mut lines = stages
                    .iter()
                    .enumerate()
                    .map(|(i, stage)| {
                        let next_indent = indent.to_string() + get_indent(is_last);
                        stage.format(&next_indent, get_prefix(i, len, false), i + 1 == len)
                    })
                    .collect::<Vec<String>>();
                let this_stage = ProfileTree::Stage {
                    name: name.to_string(),
                    time: self.time(),
                };
                lines.insert(0, this_stage.format(indent, prefix, false));
                lines.join("\n")
            }
        }
    }
}

impl fmt::Display for ProfileTree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.format("", "", false))
    }
}

fn get_indent(is_last: bool) -> &'static str {
    if is_last {
        "   "
    } else {
        "│  "
    }
}

fn get_prefix(idx: usize, len: usize, is_root: bool) -> &'static str {
    if is_root {
        if len == 1 {
            // single
            "── "
        } else if idx == 0 {
            // start
            "┌─ "
        } else if idx + 1 == len {
            // end
            "└─ "
        } else {
            // middle
            "├─ "
        }
    } else {
        if len == 1 {
            // single
            "└─ "
        } else if idx == 0 {
            // start
            "├─ "
        } else if idx + 1 == len {
            // end
            "└─ "
        } else {
            // middle
            "├─ "
        }
    }
}

pub trait ProfilableStage: Stage {
    fn profiled_exec(
        &mut self,
        ctx: &mut DemoContext,
        input: Self::Input,
    ) -> (Self::Output, ProfileTree);
}

impl<T: Stage + fmt::Display> ProfilableStage for T {
    fn profiled_exec(
        &mut self,
        ctx: &mut DemoContext,
        input: Self::Input,
    ) -> (Self::Output, ProfileTree) {
        let st = Instant::now();
        let out = self.exec(ctx, input);
        ctx.gl.finish();
        let elapsed = st.elapsed();

        let ms = (elapsed.as_secs() as f64) * 1000.0;
        let sub_ms = f64::from(elapsed.subsec_nanos()) / 1e6;

        (
            out,
            ProfileTree::Stage {
                name: format!("{}", self),
                time: ms + sub_ms,
            },
        )
    }
}

impl<T, U> ProfilableStage for CombinedStage<T, U>
where
    T: ProfilableStage,
    U: ProfilableStage<Input = T::Output>,
{
    fn profiled_exec(
        &mut self,
        ctx: &mut DemoContext,
        input: Self::Input,
    ) -> (Self::Output, ProfileTree) {
        let (this_out, this_profile) = self.this.profiled_exec(ctx, input);
        let (next_out, next_profile) = self.next.profiled_exec(ctx, this_out);

        (next_out, this_profile.merge(next_profile))
    }
}

impl<T: ProfilableStage> ProfilableStage for SubStage<T> {
    fn profiled_exec(
        &mut self,
        ctx: &mut DemoContext,
        input: Self::Input,
    ) -> (Self::Output, ProfileTree) {
        let (output, profile) = self.stage.profiled_exec(ctx, input);
        (
            output,
            ProfileTree::MultiStage {
                name: self.name.clone(),
                stages: profile.into_vec(),
            },
        )
    }
}
